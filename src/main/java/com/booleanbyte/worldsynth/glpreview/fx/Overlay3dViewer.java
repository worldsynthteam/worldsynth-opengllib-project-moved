package com.booleanbyte.worldsynth.glpreview.fx;

import com.booleanbyte.worldsynth.glpreview.heightmap.HeightmapOverlay3DGLPreviewBufferedGL3;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;

public class Overlay3dViewer extends BorderPane {
	
	private final HeightmapOverlay3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Overlay3dViewer() {
		openGL3DRenderer = new HeightmapOverlay3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setHeightmap(float[][] heightmap, float[][][] colormap, float maxHeightUnit, double size) {
		openGL3DRenderer.setHeightmap(heightmap, colormap, maxHeightUnit, size);
	}
}
