package com.booleanbyte.worldsynth.glpreview.fx;

import com.booleanbyte.worldsynth.glpreview.featurespace.Featurespace3DGLPreviewBufferedGL3;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;

public class Featurespace3dViewer extends BorderPane {
	
	private final Featurespace3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Featurespace3dViewer() {
		openGL3DRenderer = new Featurespace3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setFeaturespace(double[][] points, double x, double y, double z, double width, double height, double length) {
		openGL3DRenderer.setFeaturespace(points, x, y, z, width, height, length);
	}
}
