package com.booleanbyte.worldsynth.glpreview.fx;

import com.booleanbyte.worldsynth.glpreview.voxel.Blockspace3DGLPreviewBufferedGL3;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;

public class Blockspace3dViewer extends BorderPane {
	
	private final Blockspace3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Blockspace3dViewer() {
		openGL3DRenderer = new Blockspace3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setBlockspace(int[][][] blockspaceMaterialId) {
		openGL3DRenderer.setBlockspace(blockspaceMaterialId);
	}
}
