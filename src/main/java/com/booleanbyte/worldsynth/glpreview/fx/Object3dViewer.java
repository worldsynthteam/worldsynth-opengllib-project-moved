package com.booleanbyte.worldsynth.glpreview.fx;

import com.booleanbyte.worldsynth.customobject.CustomObject;
import com.booleanbyte.worldsynth.glpreview.voxel.Object3DGLPreviewBufferedGL3;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;

public class Object3dViewer extends BorderPane {
	
	private final Object3DGLPreviewBufferedGL3 openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Object3dViewer() {
		openGL3DRenderer = new Object3DGLPreviewBufferedGL3();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setObject(CustomObject object) {
		openGL3DRenderer.setObject(object.blocks, object.width, object.height, object.length, object.xMin, object.yMin, object.zMin, object.xMax, object.yMax, object.zMax);
	}
}
