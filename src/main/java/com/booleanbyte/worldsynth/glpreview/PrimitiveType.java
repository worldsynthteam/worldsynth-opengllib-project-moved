package com.booleanbyte.worldsynth.glpreview;

public enum PrimitiveType {
	POINTS,
	LINES,
	TRIFACES,
	QUADFACES;
	
	private PrimitiveType() {
	}
}
