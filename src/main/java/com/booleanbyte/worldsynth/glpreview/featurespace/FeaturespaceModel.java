package com.booleanbyte.worldsynth.glpreview.featurespace;

import com.booleanbyte.worldsynth.glpreview.Point;
import com.booleanbyte.worldsynth.glpreview.model.AbstractPointModel;

public class FeaturespaceModel extends AbstractPointModel {
	
	public double[][] points;
	
	private int pointsCount = 0;
	
	public FeaturespaceModel(double[][] points, double x, double y, double z, double width, double height, double length) {
		this.points = points;
		
		generatePoints(points, x, y, z, width, height, length);
	}
	
	@Override
	public int getPrimitivesCount() {
		return pointsCount;
	}
	
	private void generatePoints(double[][] points, double x, double y, double z, double width, double height, double length) {
		pointsCount = points.length;
		initVertexArray(pointsCount);
		
		Point tempPoint = new Point();
		for(int i = 0; i < pointsCount; i++) {
			tempPoint.setVertex((float) (points[i][0]-x), (float) (points[i][1]-y), (float) (points[i][2]-z));
			tempPoint.setColor(1.0f, 1.0f, 1.0f);
			insertVertexArray(tempPoint, i);
		}
	}
}
