#version 150

uniform float lightIntensity;

in vec4 Color;
in vec4 Light;

out vec4 outColor;

void main()
{
    outColor = Color * lightIntensity;
}