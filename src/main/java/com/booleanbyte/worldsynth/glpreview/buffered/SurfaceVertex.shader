#version 150

uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform vec4 lightPosition;

in vec4 in_position;
in vec4 in_color;
in vec4 in_normal;

out vec4 Color;
out vec4 Normal;
out vec4 Light;

void main()
{
    Color = in_color;
    Normal = in_normal;
    
    Light = lightPosition - in_position;
    
    gl_Position = projMatrix * viewMatrix * in_position ;
}