#version 150

uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform vec4 lightPosition;

in vec4 in_position;
in vec4 in_color;

out vec4 Color;
out vec4 Light;

void main()
{
    Color = in_color;
    
    Light = lightPosition - in_position;
    
    gl_Position = projMatrix * viewMatrix * in_position ;
}