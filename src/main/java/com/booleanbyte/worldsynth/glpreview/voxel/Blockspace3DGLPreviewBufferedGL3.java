package com.booleanbyte.worldsynth.glpreview.voxel;

import com.booleanbyte.worldsynth.glpreview.buffered.BufferedGL3Panel;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

public class Blockspace3DGLPreviewBufferedGL3 extends BufferedGL3Panel {
	private static final long serialVersionUID = -1895542307473079689L;
	
	private VoxelModel voxelModel;
	
	public Blockspace3DGLPreviewBufferedGL3() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setBlockspace(int[][][] blockspaceMaterialId) {
		voxelModel = new VoxelModel(blockspaceMaterialId);
		
		startNewModel();
		loadModel(voxelModel);
		endNewModel();
		display();
	}
}
