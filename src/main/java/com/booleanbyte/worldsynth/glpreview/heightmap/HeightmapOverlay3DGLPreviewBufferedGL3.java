package com.booleanbyte.worldsynth.glpreview.heightmap;

import com.booleanbyte.worldsynth.glpreview.buffered.BufferedGL3Panel;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

public class HeightmapOverlay3DGLPreviewBufferedGL3 extends BufferedGL3Panel {
	private static final long serialVersionUID = -1895542307473079689L;
	
	private HeightmapModel heightmapModel;
	
	public HeightmapOverlay3DGLPreviewBufferedGL3() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL3);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setHeightmap(float[][] heightmap, float[][][] colormap, float maxHeightUnit, double size) {
		heightmapModel = new HeightmapModel(heightmap, colormap, maxHeightUnit, size);
		
		startNewModel();
		loadModel(heightmapModel);
		endNewModel();
		display();
	}
}