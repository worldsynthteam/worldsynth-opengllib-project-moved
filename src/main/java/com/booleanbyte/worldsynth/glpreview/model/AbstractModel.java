package com.booleanbyte.worldsynth.glpreview.model;

import com.booleanbyte.worldsynth.glpreview.Primitive;
import com.booleanbyte.worldsynth.glpreview.PrimitiveType;

public abstract class AbstractModel<T extends Primitive> {
	
	protected float[] vertexPositionArray;
	protected float[] vertexColorArray;
	protected float[] vertexNormalArray;
	
	public abstract PrimitiveType getPrimitivesType();
	public abstract int getPrimitivesCount();
	
	protected abstract void initVertexArray(int primitivesCount);
	protected abstract void insertVertexArray(T primitive, int index);
	
	public float[] getVertexPositionArray() {
		return vertexPositionArray;
	}
	
	public float[] getVertexColorArray() {
		return vertexColorArray;
	}
	
	public float[] getVertexNormalArray() {
		return vertexNormalArray;
	}
}
