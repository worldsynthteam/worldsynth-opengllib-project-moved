package com.booleanbyte.worldsynth.glpreview;

public class Line extends Primitive {
	public float[] vertices = new float[6];
	public float[] color = new float[6];
	
	public void setVertex(int i, float x, float y, float z) {
		vertices[i*3 + 0] = x;
		vertices[i*3 + 1] = y;
		vertices[i*3 + 2] = z;
	}
	
	public void setColor(int i ,float r, float g, float b) {
		color[i*3 + 0] = r;
		color[i*3 + 1] = g;
		color[i*3 + 2] = b;
	}
	
	public void setColor(float r, float g, float b) {
		for(int i = 0; i < 2; i++) {
			setColor(i, r, g, b);
		}
	}
}
